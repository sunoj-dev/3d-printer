// PRUSA Mendel
// Configuration file
// GNU GPL v3
// Josef Průša
// josefprusa@me.com
// prusadjs.cz
// http://www.reprap.org/wiki/Prusa_Mendel
// http://github.com/prusajr/PrusaMendel

// PLEASE SELECT ONE OF THE CONFIGURATIONS BELOW
// BY UN-COMMENTING IT

// Uncomment for metric settings
// METRIC METRIC METRIC METRIC METRIC METRIC METRIC METRIC

include <metric.scad>;

// CUSTOM CUSTOM CUSTOM CUSTOM CUSTOM CUSTOM CUSTOM CUSTOM CUSTOM






thin_wall = 3;

// LM8UU
linear = false;


// CHANGE ONLY THE STUFF YOU KNOW
// IT WILL REPLACE DEFAULT SETTING

// RODS

// threaded_rod_diameter = 0;
// threaded_rod_diameter_horizontal = 0;
// smooth_bar_díameter = 0;
// smooth_bar_díameter_horizontal = 0;

// Nuts and bolts


//////////////////////////////////////////////////
// jrr changes
layer_height=0.25;

m3_nut_diameter = 5.7; // for Lowes goofy 7/32" nuts
//m3_nut_diameter = 9.85; // for standard 5/16" nuts
m3_diameter = 4.3;
m3_nut_height = 3.95;

// we'll use #6 for everything.
m4_nut_diameter = 9.85;
m4_diameter = 4.3;
m3_nut_diameter_horizontal = m3_nut_diameter + 0.1;

m8_nut_diameter = 15;  // works for M8 and 5/16 ( a little tighter on M8)
// end JRR changes
//////////////////////////////////////////////////

m8_diameter = 8.2;
m8_nut_diameter = 13.4;

m4_diameter = 4.5;
m4_nut_diameter = 8.5;

m3_diameter = 3.5;
m3_nut_diameter = 6.6;

// Bushing holder

// bushing_core_diameter = smooth_bar_díameter;
// bushing_material_thickness = 0;


///counted stuff
m3_nut_diameter_bigger = ((m3_nut_diameter  / 2) / cos (180 / 6))*2;

// functions 
include <functions.scad>

