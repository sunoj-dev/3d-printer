                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


https://www.thingiverse.com/thing:2366523
Anet Extruder Filament Guide by Leo_N is licensed under the Creative Commons - Attribution - Non-Commercial license.
http://creativecommons.org/licenses/by-nc/3.0/

# Summary

This extruder filament guide is for the Anet printers when needing to print flexible filaments such as TPU, Ninjaflex, Filaflex, etc...
The part prevents the flexible filament from escaping or deforming during the extrusion process and tangle inside the extruder gear.

This is declared as a remix. However this design is made from scratch since the original design by MasterFX didn't fit my printer perfectly.
I've also added some improvements and 4 different versions to accommodate different heights concerning placement of the hot end throat. However I would recommend that the hot end throat be placed even with the mounting plate so that the heater block is furthest possible distance from the extruder carriage.
I also found the idea from Bomber0 with the added rise very good and integrated it into my design. 

Offered are files for different heights depending how much the Teflon nozzle throat is protruding over the holder block.

For installation remove the spring and the nut, place the guide over the shaft and slide it under the gear. Lightly retighten the nut. Make sure the filament can freely move after having mounted the guide.

Please post a picture of your finished print with comments of likes and/or dislikes.

Thanks.

Leo

  

# Print Settings

Printer: Anet A8
Rafts: Doesn't Matter
Supports: No
Resolution: 0.16
Infill: 100%

Notes: 
This item should be printed in either PETG or ABS.
PLA will also work if you have the extruder fan running at all times.

I strongly recommend that you carefully enlargen the hole for the filament with a 2.0 - 2.2mm drill bit.
This will give a clean even surface inside the hole and the 1.75mm filament enough freedom of movement.