                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:1872162
Anet A8 Brackets to Reduce X-Axis Motion by Leo_N is licensed under the Creative Commons - Attribution - Non-Commercial license.
http://creativecommons.org/licenses/by-nc/3.0/

# Summary

These brackets are designed for the Anet A8 printer with the goal of reducing X-axis motion particularly when printing in higher reigns of the printer.
To mount the brackets you will need to dismantle the display screen and remove the 4 pillar washers as they will not be needed. Place each bracket in the corners of the frame, remount the display screen and additionally fixate the brackets to the side support plates with M3x0 mm screws. Make sure the brackets are placed flat on the frame plates. It might be necessary to clean up the holes in the brackets to get a clean fit, depending on the quality of the print and the tolerances of your frame.

I've designed the brackets. One is for the right side of the frame. The other is for the left side. I've designed the left version with a filament guide and one without.

Please let me know if you have a problem with the design so I can improve it if necessary or you what new features to the design.

Also please post a picture with comments if you have made one.

Thanks.

Leo

<H5>Update 2016.11.21</H5>
- For those wanting to use a spool holder on top the frame with these brackets may want to try these: <a rel="nofollow" href="http://www.thingiverse.com/thing:1908880">http://www.thingiverse.com/thing:1908880</a>

<H5>Update 2016.11.22</H5>
- I've added a bigger filament guide as stl file for the left bracket. Just stick it on top.

<H5>Update 2017.01.15</H5>
- User "BlackChart" had asked if I could add a panel to the right bracket. He provided the part as sketch for me. You can mount an ON/OFF switch and a switch for lights. The holes have a diameter of 5mm and can be enlarged by drilling a bigger hole.

<H5>Update 2017.05.16</H5>
- For those wanting the ultimate solution: http://www.thingiverse.com/thing:2189694

# Print Settings

Printer: Anet A8
Rafts: No
Supports: No
Resolution: 0.25
Infill: 25%

Notes: 
The 3D prints were made at 50mm/s with 0.8mm or 1.2 mm shell thickness and 0.8mm top / bottom thick closures.