                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:1884480
Anet A8 Y-Undercarriage 4x LM8UU with Belt-Tensioner by FZS1_Fazer is licensed under the Creative Commons - Attribution - Non-Commercial - Share Alike license.
http://creativecommons.org/licenses/by-nc-sa/3.0/

# Summary

I've made an Y-undercarriage for 4 standard LM8UU bearings, for the Y-belt is a belt tensioner intigrated, all holes and screws are for M3 dia with 5.5mm nut wrench size.
There is a low version to use with the original acrylic panel or a H shaped base of at least 3mm thickness. The mountingholes are made to be able to build the H of three pieces of anodized flat aluminium profile 50mm wide and 4mm thick.
With the higher version you can use a square panel of your choice. ;)
The scad files were only the base for creating this thing and you will see that I've added some parts after creating the thing with OpenSCAD.
The H is only a sample how the profile should be arranged.
In version 1 and 2 is the only difference the arrangement of the small parts.

Btw. the print size is 191,5mm x 190mm, so you shouldn't have any problems with that.

The Y endstop switch has to be replaced.

I made one of the lower version with PLA in 0.25mm resolution and it's fits very well on my Anet A8 B.

For this thing, PLA is recommended caused by it's higher rigidity.

EDIT:
First version was for an H made of 50mm x 4mm profile cause the online catalog said that a local market has exactly that stuff, but the catalog was faulty and all I could find was 50mm x 3mm. I bought it for a test build and the result is stiff enough but the measurments doesn't work, also the belt tensioner that I took from the original design that I remixed was going too deep, so I had to change the measurements of it too.
I've uploaded these first corrections as v2, hoping that it'll work as intended.

I will do a test print know and report if it's done or not.