// PRUSA Mendel
// LM8UU-Bearing X-Carriage
// Used for sliding on Y axis
// GNU GPL v2
//
// 
// Derived from:
//	- "Y-Undercarriage" by sgk07072000
//	  http://www.thingiverse.com/thing:28361
//	Derived from: Simon Kühling <mail@simonkuehling.de>
//		Derived from
//		- "Lm8uu X Carriage with Fan Mount for Prusa Mendel" by Greg Frost
//		  http://www.thingiverse.com/thing:9869

// Gregs configuration file
include <configuration.scad>

include <LM8UU_holder_ziptie.scad>

SupportThickness=0.25; // Z thickness of support struts
SupportWidth = 10; // width of main central support struts
ClampSeparation = 38; // Y distance between belt clamps

// overall physical dimensions
xWidth = 170;
yWidth = 75;
height = 14;

// Belt characteristics for clamp
belt_width=7.25;
belt_thickness=2;
tooth_height=1;
tooth_offset = 2; // width of a tooth plus a gap - 2 for GT2, 5 for XL/T5
belt_distance_from_bed = 8; // how far from the bottom of your print surface to the TOP of your belt

belt_clamp_thickness=5;
belt_clamp_spacing=8;	// The spacing between the belt clamps
belt_clamp_hole_separation=belt_width + m3_nut_diameter + 2;

layer_thickness = 0.25; // print layer thickness, 0.33 should be OK for 0.25 as well

ClampWidth = belt_clamp_hole_separation + m3_nut_diameter;


belt_clamp_width = m3_nut_diameter * 1.5;
belt_clamp_length = belt_clamp_hole_separation + belt_clamp_width + m3_diameter;

main();

module main()
	{
	difference()
		{
		structure();
		holes();
		}
	}

module structure()
       {
	// This is mirrored on the X axis, this for statement makes one of everything either side of center
	for(i=[-1,1])
		{
		// outermost support rail, outside edge of the bearing holders
		translate([i*((xWidth / 2) + (body_width / 2) - (SupportThickness / 2)), 0, (SupportThickness / 2)])
			cube([SupportThickness, yWidth - body_length, SupportThickness], center = true);

		// another support rail, just inside the bearing holders
		translate([i*((xWidth / 2) - (body_width / 2) + (SupportThickness / 2)), 0, (SupportThickness / 2)])
			cube([SupportThickness, yWidth - body_length, SupportThickness], center = true);

		// the supports between the two Y rods, through the belt clamps
		for (j=[-0.5,0.5])
		translate([j*((xWidth/2)+(ClampWidth/2) + (body_width/2)), i * (ClampSeparation/2), (SupportThickness / 2)])
			cube([((xWidth / 2) - (ClampWidth/2) + (body_width/2)), SupportWidth, SupportThickness], center = true);

		// the support bar that runs between the belt clamps just outside of center
		translate([i * (ClampSeparation / 2 - 6), 0, (SupportThickness / 2)])
			cube([SupportWidth + 2, ClampSeparation - 7.3, SupportThickness], center = true);

		for (j=[-1,1])
		{
		translate([j*xWidth/2,i*(yWidth/2),0])
			rotate([0,0,90])
				LM8UU_holder();
		}
		}

	// the belt attachment points with the tightener
	translate([0, 1 * (ClampSeparation / 2), 0])
			belt_clamp(height=belt_distance_from_bed + belt_thickness, nut_holes=1, belt_channel=1);

	// the belt attachment points without the tightener
	translate([0, -1 * (ClampSeparation / 2), 0])
			belt_clamp(height=belt_distance_from_bed, nut_holes=1);

	// make clamps that are separate from the structure
	// the the center piece with the teeth and the tightener
	translate([ClampSeparation, ClampSeparation, 0])
		belt_clamp(teeth=1, tightener=1, height=m3_nut_diameter + tooth_height);
	// the cap with no features (top of the tightener side)
	translate([0, ClampSeparation, 0])
		belt_clamp();
	// the cap with teeth (non-tightener side)
	translate([0, -1 * ClampSeparation, 0])
		belt_clamp(teeth=1);

	// belt rams
	translate([-ClampSeparation, ClampSeparation, 0])
		belt_ram();
	}

module holes()
{
	for(i= 0)
	{
		for (j=[-0.35,0.35])
		{
		translate([j*ClampSeparation, i * ClampSeparation / 2, -0.05])
			cylinder(r=m3_diameter/2,h=SupportThickness+0.1,$fn=16);
		}
	}
    for(i=[-1,1])
	{
		for (j=[-1.7,1.7])
		{
		translate([j*ClampSeparation, i * ClampSeparation / 2, -0.05])
			cylinder(r=m3_diameter/2,h=SupportThickness+0.1,$fn=16);
		}
	}
    for(i=[-1,1])
	{
		for (j=[-1.4,1.4])
		{
		translate([j*ClampSeparation, i * ClampSeparation / 2, -0.05])
			cylinder(r=m3_diameter/2,h=SupportThickness+0.1,$fn=16);
		}
	}
}


module belt_clamp(height=belt_clamp_thickness, nut_holes=0, belt_channel=0, tightener=0, teeth=0)
	{
	difference()
		{
		translate([0, 0, height / 2])
			union()
				{
				// the central body
				cube([belt_clamp_hole_separation, belt_clamp_width, height], center = true);
				// the two rounded sides
				for(i = [-1, 1])
					translate([i * belt_clamp_hole_separation / 2, 0, 0])
						cylinder(r = belt_clamp_width / 2, h = height, center = true);
				}

		// holes
		union ()
			{
			// screw and nut holes
			for(i=[-1, 1])
				{
				// make nut holes if requested
				if (nut_holes > 0)
					{
					translate([i * belt_clamp_hole_separation / 2, 0, 0])
						rotate(360 / 12)
							cylinder(r = m3_nut_diameter / 2, h = m3_nut_height, $fn = 6);

					// move this up a bit to make a bridging layer
					translate([i * belt_clamp_hole_separation / 2, 0, m3_nut_height+layer_thickness])
						rotate(360 / 16)
							cylinder(r = m3_diameter / 2, h = height-m3_nut_height, $fn = 16);
					}
				else
					{
					translate([i * belt_clamp_hole_separation / 2, 0, -.05])
						rotate(360 / 16)
							cylinder(r = m3_diameter / 2, h = height+.1, $fn = 16);
					}
				}
			// belt channel if requested
			if (belt_channel > 0)
				{
				translate([0,0,height - belt_thickness/2])
	 			cube([belt_width, belt_clamp_width, belt_thickness], center=true);
				}
			// teeth if requested
			if (teeth > 0)
				{
				for(i=[0:belt_clamp_width/tooth_offset])
					{
					translate([0, (-belt_clamp_width / 2) + (tooth_offset * i), (height - tooth_height/2)])
						cube([belt_width, tooth_offset/2, tooth_height], center=true);
					}
				}
			// tightener if requested
			if (tightener > 0)
				{
				translate([0,belt_clamp_width/2+0.05, m3_nut_diameter/2])
					rotate([90,0,0])
						union() 
							{
							cylinder(r=m3_nut_diameter/2, h=m3_nut_height+0.1, $fn=6);
							cylinder(r=m3_diameter/2, h=belt_clamp_width+0.1, $fn=16);
							}
				}
			}
		}
	}

module belt_ram()
	{
	difference()
		{
		union()
			{
			translate([0, -nut_wrench_size / 3, 0])
				cube([nut_wrench_size, nut_wrench_size / 1.5, nut_wrench_size]);
			translate([0, 0, nut_wrench_size])
				rotate([0, 90, 0])
					cylinder(r = nut_wrench_size / 3, h = nut_wrench_size, $fn = 20);
			}
		translate([nut_wrench_size / 2, 0, 0])
			cylinder(r = screw_thread_dia / 2 - 0.2, h = nut_wrench_size, $fn = 16);
		}
	}