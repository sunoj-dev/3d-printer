                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:2050229
ANET A8 Z Endstop fine adjustment by ibschreiber is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

First of all, the original design is ingenious! Thank you Meermeneer!

I did change two things: 
- The Leg has a 2.95 mm hole for 3mm screws which gives more stability, no more drilling or heating needed anymore. 
- The Foot is completely redesigned also for 3mm screw and has a big flat bottom and is easy to screw on to the screw and is used for calibration as a turning knob ;-)

Assembly instructions:
- Mount the switch holder to the frame, add the switch
- put a M3x35 or similar through the legs hole
- add the foot to the screw, tighten it a little bit, not too much or you'll deform the bottom surface. You can glue the screw into the foot to give it a better hold. 

Calibration: 
just turn the screw using the six sided foot, enjoy!

Note:
A good calibrated printer is essential for the screws to fit the holes tightly!

If it doesn't fit, just scale the model up 1-3% ;-)

For a perfectly leveled heatbed, take a look at http://www.thingiverse.com/thing:2044801

To set up your z axis, just remember: one complete turn of the M3 screw is 0.5 mm