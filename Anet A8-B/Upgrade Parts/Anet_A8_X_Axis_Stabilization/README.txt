                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:2071984
Anet A8 X Axis Stabilization by danohpsp is licensed under the Creative Commons - Attribution - Non-Commercial - Share Alike license.
http://creativecommons.org/licenses/by-nc-sa/3.0/

# Summary

The Anet A8 is a fun unit but it has a lot of problems that need to be tinkered with and improved.  On my unit, I noticed that if I grabbed the metal U bracket that holds the extruder to the X axis and rotated it clockwise and counter-clockwise, that there was a mile of play and wobble.  I knew that the bearings that came with the A8 are total garbage, so I first upgraded all three bearings on the X Axis with some quality German 8 x 15 x 24 linear bearings (Ina Model KH0824PP).  I also replaced the 8mm diameter rails with two quality rails that I ordered from McMaster Carr (400 mm long-McMaster Carr part number 6112K45). Printer got quieter in the X axis and was smoother operating. That improved the printing situation a little, but there was still a problem.  I found out with some research later that these type of bearings do allow a degree or less of movement to compensate for alignment issues.  I figured that if I could stretch out the distance between the upper bearings, the mounting points on the X axis would be a larger triangle that would have less play.  So I designed an extended pillow block, that can do just that.  Thinking back, using bronze bushings might have worked also, but even with those, these blocks could be helpful.  Because the blocks are extended and if you do decide to do this mod to your printer, you might have to tweak the X axis offset numbers in your software.  Because the blocks are only extended by 2 cm, you still have full axis to the left side of the printing table and almost to the edge on the right side. The blocks separate the upper bearings by an extra 4cm over the existing spacing.  After doing the mod, I found out that printing is way more even, especially in the X axis direction and have noticed a dramatic improvement in printing quality. You will notice MUCH less play in the head after doing this mod, and I will include a before-and-after picture showing the changes in the print. The print of the Z axis screw clamp was done before the mod, and the print of the elephant (for the kids) was made directly after.  Big difference in print quality. Because the new pillow blocks barely hit the X axis limit switch on homing, and I did not like the way the wires routed to the switch, I have also included a quick design for an X axis limit switch mounting bracket.  That bracket uses the existing mounting holes on the unit and you should have enough extra hardware in the kit to find two extra wood screws that are used to mount the bracket and the switch from the original materials. Lastly, belt tensioners are a must on the A8, and I am sure that one of the many designs available on Thingiverse will work in your setup.  If you try this mod out, please provide your feedback and post your results as I am curious of your results!  Now...onto the Y Axis!

FYI: I added four pictures.  One is the diameter measurement of the Chinese made rail, the other of a McMaster Carr qualified rail.  You can see that the Chinese rail is undersized by over .0015"!  Also, two pictures of the diameter of a Chinese bearing vs the German one...the German bearing is oversized by almost .0025 and the Chinese bearing is just under 15mm.  That is why the Chinese bearings slide right out of the pillow blocks once the C clip is removed.  Who knows how close the hole in the blocks are...more tolerance everywhere...total junk but easy to upgrade with Ebay parts and just a few dollars.

# Print Settings

Printer: Anet A8
Rafts: Yes
Supports: Yes
Resolution: .3mm
Infill: 30%

Notes: 
Printed the switch bracket with 2 layer walls and the pillow blocks with 4 layer walls.  .3mm height works great for both.  The blocks were printed with the rod clearance hole (screw side of the block) down on the table without supports.  The switch bracket was printed with supports initially, but has been redesigned to print without supports.

# Post-Printing

On the pillow blocks, you can chose to drill out the four mounting holes with a 9/64" drill and then self-tap the 4mm mouting screws into the block, or you can drill clearance holes and optionally use the hexagonal nut profile to retain a 4mm nut into the block.  I used the self-tap method which worked fine, but might I might change the setup in the future.  The nut feature was added in a later revision of my design after further thinking about the design.