                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


https://www.thingiverse.com/thing:2454397
Anet A8 Y-Axis Middle Support by Indychus is licensed under the Creative Commons - Attribution - Share Alike license.
http://creativecommons.org/licenses/by-sa/3.0/

# Summary

The long unsupported length of threaded rods in the middle of my A8 bothered me.  I made this simple brace to finish out the bottom of the printer and add some rigidity to the threaded rods.  It has a hex pattern to match front/rear braces found on here by other users.  This probably doesn't do much, but it looks cool and may lend some extra stiffness to the Y axis. Note that it does not touch whatever surface your printer sits on; it is intended to clamp to the Anet frame via the nuts and support the threaded rods. 

My printer is called "Archimedes", hence the text.  The STL I uploaded does not have this text.

The brace is split in half so each half will fit on the A8 build platform at a 45 degree angle.  Print the 2 halves one at a time, flat on their bottom with no supports and 30% hex infill.  

Install as pictured and snug the nuts up pretty good.

I used Solidworks for the design and Simplify3D to slice.  

# Print Settings

Printer Brand: Prusa
Printer: Prusa Clone
Rafts: No
Supports: No
Resolution: .15
Infill: 30% Hex

Notes: 
Print flat on the base at a 45 degree angle to fit on the A8 platform.  You need 2 of them!