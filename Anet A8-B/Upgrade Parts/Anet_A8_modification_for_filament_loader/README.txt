                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:2037833
Anet A8 modification for filament loader by nicolaidiz is licensed under the Creative Commons - Attribution - Non-Commercial license.
http://creativecommons.org/licenses/by-nc/3.0/

# Summary

I did this modification because the changing of filament is a pain in the ...

If you don´t have any troubles, you don´t need this modi at all ;)

See it in action: https://youtu.be/wS0gAGX58vc

In this modi the cooling fan is a seperate block and can be opened for better view to the extruder while changing or to get easier access if anything is jammed.

The mechanism is not yet perfect... All the parts have tolerances that means you have to align your system once an than fix it.

UPDATE Cover design 20.01.2017
(New update! But for this you can still use the parts before update!)

I did this update for better cooling. For this solution the position of the heatsink is given but then you have to cut your heatsink after the fourth rib (see picture). The size of the heatsink is enough for cooling the buttom block. You can add thermal paste as well.

I did a 5 hours print -> the block was almost cold!

Fix the Motor and heatsink with two srews on the bottom left/right hole. Fix the cooler block assembly. It´s even possible to fix the block by 4 screws.
For the clamping screw I used a M3x25 a washer and two Nuts. Additionally you can use a spring of a standard biro.

That´s it, and if you want to cool the heatsink more efficient you can add two holes on the long top rib or turn the heatsink by 180 degree.

Thats for sure an option to handle the whole thing easier ;)
If you have any doubts, let me know.

Enjoy

Bill of material

2x M3x20 (heat sink)
3x M3x25
4x M3 Nuts
1x M3 self locking nut for the hinge (if available)
1x M3x40 (hinge)
2x M4x15 (20)
1x M3 washer 
1x Spring of a ball pen

# Print Settings

Printer: Anet A8
Rafts: Doesn't Matter
Supports: Yes
Resolution: 0,2
Infill: 75