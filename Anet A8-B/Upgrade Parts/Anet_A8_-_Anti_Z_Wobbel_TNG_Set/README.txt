                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:2067676
Anet A8 - Anti Z Wobbel TNG (Set) by OderWat is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

After printing the original anti-z wobble for the Anet A8 I found out that it is a bit to small to fit the original screws. It also is a combination of two other volumetric objects which does not slice really well with Simplify 3D.

Because I had no bearings at head I printed some dust caps for the z-axis hole in the top of the frame. To make them a guide for the z-axis I heavily scaled their original design.

Now as those stuff is working fine I thought I create a set for this modification which prints out of the box without any hassle. After more than 20 iterations I think I am done now!

I also included the raw fusion 360 files for anybody who wants to change the design. 

My version does not use the original models but for reference. It was build from ground up and later even changed quite substantial. Many thanks for all people which experimented with that kind of mod!

Update: Fixed the model so it will be sliced by Slic3r, Cura and Simplify3D

# Print Settings

Printer: Anet A8
Rafts: No
Supports: No
Resolution: 0.2 mm
Infill: 40%

Notes: 
It is meant to be printed it with 3 perimeters and 6 shells which is the same as printing with 1,2 mm wall and surface thickness. It is hard to design in a way that everybody can print it. I hope after about 20 iterations the models fit for most people out of the box!